<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('basket_id')->unsigned();
			$table->enum('type', ['apple', 'orange', 'watermelon']);
			$table->float('weight')->default(0);

			$table->timestamps();


			$table->foreign('basket_id')
				->references('id')
				->on('baskets')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
