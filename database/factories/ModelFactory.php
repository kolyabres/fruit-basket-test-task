<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Basket::class, function (Faker\Generator $faker) {
	return [
//		'id'=> rand(1,10),
		'name' => $faker->name,
		'max' => 3,
	];
});


$factory->define(App\Item::class, function (Faker\Generator $faker) {
	$types = ['orange', 'apple', 'watermelon'];
	return [
//		'id'=> rand(1,10),
		'type' => $types[rand(0, 2)],
		'weight' => 1,
	];
});


