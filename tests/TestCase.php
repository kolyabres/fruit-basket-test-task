<?php
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Basket;
use App\Item;
class TestCase extends Illuminate\Foundation\Testing\TestCase
{

    use DatabaseMigrations;
    use DatabaseTransactions;
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';


    protected $structures = [
        'basket' => [
            'body' => [
                'id',
                'name',
                'max',
                'items' => [[
                    'id',
                    'type',
                    'weight',
                ]],
            ],
            'message',
            'code'
        ]
    ];




    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    protected function getFactory()
    {
        return factory(App\Basket::class, 3)
            ->create()->each(function ($b) {
                $b->items()->save(factory(App\Item::class)->make());
            })->first();
    }
}
