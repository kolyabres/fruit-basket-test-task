<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Basket;

class ItemTest extends TestCase
{


	public function testStoreBadTypeAction()
	{


		$basket = $this->getFactory();

//		var_dump($basket->first()->id);die;
		$result = $this->json('POST', "/api/v1/baskets/{$basket->id}/items", [
			'items' => [
				['type' => 'fsdfsdf', 'weight' => 1],
			]
		]);
		$result->receiveJson([
			'code' => 400,
			'message' => 'Bad request.',
		])->assertResponseStatus(400);

	}

	public function testStoreAction()
	{

		$basket = $this->getFactory();
		$result = $this->json('POST', "/api/v1/baskets/{$basket->id}/items", [
			'items' => [
				['type' => 'orange', 'weight' => 1],
			]
		]);

//		echo $result->response->getContent();
		$result->receiveJson([
			'code' => 200,
			'message' => 'Items added successfully.',
		])->seeJsonStructure($this->structures['basket'])
			->assertResponseStatus(200);

	}

	public function testDestroyAction()
	{

		$basket = $this->getFactory();
		$result = $this->json('DELETE', "/api/v1/baskets/{$basket->id}/items/{$basket->items()->first()->id}");

//		echo $result->response->getContent();
		$result->receiveJson([
			'code' => 200,
			'message' => 'Item deleted successfully.',
		])->assertResponseStatus(200);

	}

}
