<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Basket;

class BasketTest extends TestCase
{

	public function testIndexAction()
	{
		$this->json('GET', '/api/v1/baskets')
			->seeJson([
				'code' => 200,
			])->assertResponseStatus(200);
	}


	public function testStoreAction()
	{
		$result = $this->json('POST', '/api/v1/baskets', [
			'name' => 'tedst',
			'max' => 3,
			'items' => [
				['type' => 'apple', 'weight' => 1],
				['type' => 'orange', 'weight' => 1],
			]
		]);
		$result->receiveJson([
			'code' => 200,
			'message' => 'Basket created successfully.',
		])->seeJsonStructure($this->structures['basket'])
			->assertResponseStatus(200);

	}

	public function testStoreBadAction()
	{
		$result = $this->json('POST', '/api/v1/baskets', [
			'max' => 2,
		]);


		$result->receiveJson([
			'code' => 400,
			'message' => 'Bad request.',
		])->assertResponseStatus(400);

	}


	public function testStoreTooHeavyAction()
	{
		$result = $this->json('POST', '/api/v1/baskets', [
			'user_id' => 1,
			'name' => str_random(10),
			'max' => 2,
			'items' => [
				['type' => 'apple', 'weight' => 1],
				['type' => 'orange', 'weight' => 10],
			]
		]);
//		echo $result->response->getContent();die;
		$result->seeJson([
			'code' => 400,
			'message' => 'Items too heavy.',
		])->assertResponseStatus(400);

	}


	public function testShowAction()
	{
		$basket = $this->getFactory();
		$result = $this->json('GET', '/api/v1/baskets/' . $basket->id);


		$result->seeJson([
			'code' => 200,
		])->seeJsonStructure($this->structures['basket'])
			->assertResponseStatus(200);
	}

	public function testUpdateAction()
	{
		$basket = $this->getFactory();
		$new_name = str_random(10);

		$result = $this->json('PUT', '/api/v1/baskets/'.$basket->id, [
			'name' => $new_name,
		]);
//		echo $result->response->getContent();

		$result->seeJson([
//			'body' => array(
//				'id' => $basket->id,
//				'items' => $basket->items()->get(),
//				'name' => $new_name,
//				'max' => $basket->max,
//			),
			'message' => 'Basket updated successfully.',
			'code' => 200,
		])->seeJsonStructure($this->structures['basket'])
			->assertResponseStatus(200);
	}


	public function testDestroyAction()
	{
		$basket = $this->getFactory();

		$result = $this->json('DELETE', '/api/v1/baskets/'.$basket->id);
//		echo $result->response->getContent();

		$result->seeJson([
			'message' => 'Basket deleted successfully.',
			'code' => 200,
		])->assertResponseStatus(200);
	}

	public function testDestroyBadAction()
	{

		$result = $this->json('DELETE', '/api/v1/baskets/fsdgfsdg2t');
//		echo $result->response->getContent();die;
		$result->seeJson([
			'message' => 'Not found.',
			'code' => 404,
		])->assertResponseStatus(404);
	}


}
