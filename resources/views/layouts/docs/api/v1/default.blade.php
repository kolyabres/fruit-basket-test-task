<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    @include('includes.docs.api.v1.head')
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-3" id="sidebar">
            <div class="column-content">
                <div class="search-header">
                    <img src="/assets/docs/api.v1/img/f2m2_logo.svg" class="logo" alt="Logo"/>
                    <input id="search" type="text" placeholder="Search">
                </div>
                <ul id="navigation">

                    <li><a href="#introduction">Introduction</a></li>


                    <li>
                        <a href="#Basket">Basket</a>
                        <ul>
                            <li><a href="#Basket_index">index</a></li>

                            <li><a href="#Basket_store">store</a></li>

                            <li><a href="#Basket_show">show</a></li>

                            <li><a href="#Basket_update">update</a></li>

                            <li><a href="#Basket_destroy">destroy</a></li>
                        </ul>
                    </li>


                    <li>
                        <a href="#Item">Item</a>
                        <ul>
                            <li><a href="#Item_store">store</a></li>

                            <li><a href="#Item_destroy">destroy</a></li>
                        </ul>
                    </li>


                </ul>
            </div>
        </div>
        <div class="col-9" id="main-content">

            <div class="column-content">

                @include('includes.docs.api.v1.introduction')

                <hr/>


                <a href="#" class="waypoint" name="Basket"></a>

                <h2>Basket</h2>

                <p></p>


                <a href="#" class="waypoint" name="Basket_index"></a>

                <div class="endpoint-header">
                    <ul>
                        <li><h2>GET</h2></li>
                        <li><h3>index</h3></li>
                        <li>api/v1/baskets</li>
                    </ul>
                </div>

                <div>
                    <p class="endpoint-short-desc">Display a listing of the resource.</p>
                </div>

                <hr>

                <a href="#" class="waypoint" name="Basket_store"></a>

                <div class="endpoint-header">
                    <ul>
                        <li><h2>POST</h2></li>
                        <li><h3>store</h3></li>
                        <li>api/v1/baskets</li>
                    </ul>
                </div>

                <div>
                    <p class="endpoint-short-desc">Store a newly created resource in storage.</p>
                </div>
                <!--  <div class="parameter-header">
                      <p class="endpoint-long-desc"></p>
                 </div> -->
                <form class="api-explorer-form" uri="api/v1/baskets" type="POST">
                    <div class="endpoint-paramenters">
                        <h4>Parameters</h4>
                        <ul>
                            <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>

                            </li>
                            <li>
                                <div class="parameter-name">name</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Basket name</div>
                            </li>
                            <li>
                                <div class="parameter-name">max</div>
                                <div class="parameter-type">float</div>
                                <div class="parameter-desc">Max basket weight</div>
                            </li>
                            <li>
                                <div class="parameter-name">items</div>
                                <div class="parameter-type">array [['type' => 'apple', 'weight' => 1],['type' => 'orange', 'weight' => 1]] type:string, weight:float</div>
                                <div class="parameter-desc">Max basket weight</div>
                            </li>

                        </ul>
                    </div>
                </form>
                <hr>

                <a href="#" class="waypoint" name="Basket_show"></a>

                <div class="endpoint-header">
                    <ul>
                        <li><h2>GET</h2></li>
                        <li><h3>show</h3></li>
                        <li>api/v1/baskets/{baskets}</li>
                    </ul>
                </div>

                <div>
                    <p class="endpoint-short-desc">Display the specified resource.</p>
                </div>
                <!--  <div class="parameter-header">
                      <p class="endpoint-long-desc"></p>
                 </div> -->
                <form class="api-explorer-form" uri="api/v1/baskets/{baskets}" type="GET">
                    <div class="endpoint-paramenters">
                        <h4>Parameters</h4>
                        <ul>
                            <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                            </li>
                            <li>
                                <div class="parameter-name">id</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc"></div>
                            </li>

                        </ul>
                    </div>
                    <div class="generate-response">
                        <!-- <input type="hidden" name="_method" value="GET"> -->
                        <input type="submit" class="generate-response-btn" value="Generate Example Response">
                    </div>
                </form>
                <hr>

                <a href="#" class="waypoint" name="Basket_update"></a>

                <div class="endpoint-header">
                    <ul>
                        <li><h2>PUT</h2></li>
                        <li><h3>update</h3></li>
                        <li>api/v1/baskets/{baskets}</li>
                    </ul>
                </div>

                <div>
                    <p class="endpoint-short-desc">Update the specified resource in storage.</p>
                </div>
                <!--  <div class="parameter-header">
                      <p class="endpoint-long-desc"></p>
                 </div> -->
                <form class="api-explorer-form" uri="api/v1/baskets/{baskets}" type="PUT">
                    <div class="endpoint-paramenters">
                        <h4>Parameters</h4>
                        <ul>
                            <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                            </li>
                            <li>
                                <div class="parameter-name">id</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc"></div>
                            </li>
                            <li>
                                <div class="parameter-name">name</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Basket name</div>
                            </li>

                        </ul>
                    </div>
                </form>
                <hr>

                <a href="#" class="waypoint" name="Basket_destroy"></a>

                <div class="endpoint-header">
                    <ul>
                        <li><h2>DELETE</h2></li>
                        <li><h3>destroy</h3></li>
                        <li>api/v1/baskets/{baskets}</li>
                    </ul>
                </div>

                <div>
                    <p class="endpoint-short-desc">Remove the specified resource from storage.</p>
                </div>
                <!--  <div class="parameter-header">
                      <p class="endpoint-long-desc"></p>
                 </div> -->
                <form class="api-explorer-form" uri="api/v1/baskets/{baskets}" type="DELETE">
                    <div class="endpoint-paramenters">
                        <h4>Parameters</h4>
                        <ul>
                            <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                            </li>
                            <li>
                                <div class="parameter-name">id</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc"></div>
                            </li>

                        </ul>
                    </div>
                </form>
                <hr>


                <a href="#" class="waypoint" name="Item"></a>

                <h2>Item</h2>

                <p></p>


                <a href="#" class="waypoint" name="Item_store"></a>

                <div class="endpoint-header">
                    <ul>
                        <li><h2>POST</h2></li>
                        <li><h3>store</h3></li>
                        <li>api/v1/baskets/{baskets}/items</li>
                    </ul>
                </div>

                <div>
                    <p class="endpoint-short-desc">Store a newly created resource in storage.</p>
                </div>
                <!--  <div class="parameter-header">
                      <p class="endpoint-long-desc"></p>
                 </div> -->
                <form class="api-explorer-form" uri="api/v1/baskets/{baskets}/items" type="POST">
                    <div class="endpoint-paramenters">
                        <h4>Parameters</h4>
                        <ul>
                            <li>
                                <div class="parameter-name">items</div>
                                <div class="parameter-type">array [['type' => 'apple', 'weight' => 1],['type' => 'orange', 'weight' => 1]] type:string, weight:float</div>
                                <div class="parameter-desc">Max basket weight</div>
                            </li>
                            <li>
                                <div class="parameter-name">basket_id</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc"></div>
                            </li>

                        </ul>
                    </div>
                </form>
                <hr>

                <a href="#" class="waypoint" name="Item_destroy"></a>

                <div class="endpoint-header">
                    <ul>
                        <li><h2>DELETE</h2></li>
                        <li><h3>destroy</h3></li>
                        <li>api/v1/baskets/{baskets}/items/{items}</li>
                    </ul>
                </div>

                <div>
                    <p class="endpoint-short-desc">Remove the specified resource from storage.</p>
                </div>
                <!--  <div class="parameter-header">
                      <p class="endpoint-long-desc"></p>
                 </div> -->
                <form class="api-explorer-form" uri="api/v1/baskets/{baskets}/items/{items}" type="DELETE">
                    <div class="endpoint-paramenters">
                        <h4>Parameters</h4>
                        <ul>
                            <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                            </li>
                            <li>
                                <div class="parameter-name">id</div>
                                <div class="parameter-type">int</div>
                                <div class="parameter-desc"></div>
                            </li>

                        </ul>
                    </div>
                </form>
                <hr>


            </div>
        </div>
    </div>
</div>


</body>
</html>
