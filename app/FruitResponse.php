<?php
/**
 * Created by PhpStorm.
 * User: kolyabres
 * Date: 02.06.16
 * Time: 14:28
 */

namespace App;


use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;


class FruitResponse extends Response
{
	public static function makeResponse($body, $message = '', $code = 200)
	{
		$response = [
			'body' => $body,
			'message' => $message,
			'code' => $code
		];


		switch (Input::get('format', 'json')) {
			case 'json': {
				return response()->json($response, $code);
			}
		}
	}
}