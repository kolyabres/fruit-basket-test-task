<?php

namespace App;


class Basket extends Base
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'max',
	];


	/**
	 * Get the basket items
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function items()
	{
		return $this->hasMany('App\Item');
	}

	public function addItemsFromRequest($items)
	{
		$items_obj = [];
		foreach ($items as $item)
			$items_obj[] = new Item($item);
		$this->items()->saveMany($items_obj);
	}


}
