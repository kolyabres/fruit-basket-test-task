<?php
/**
 * Created by PhpStorm.
 * User: kolyabres
 * Date: 27.05.16
 * Time: 14:57
 */

namespace App;

use  Validator;

class FruitValidator
{


	public static function validateBasketWeight($weight, $items)
	{
		$sum = 0;
		foreach ($items as $item)
			$sum += $item['weight'];
		return ($weight - $sum) >= 0;

	}
}