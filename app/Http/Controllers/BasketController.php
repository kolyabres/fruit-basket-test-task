<?php namespace App\Http\Controllers;

use App\FruitValidator;
use App\FruitResponse;
use App\Http\Requests\StoreBasketRequest;
use App\Http\Requests\UpdateBasketRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Basket;
use App\Item;
use Illuminate\Http\Request;

use Validator;


class BasketController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$baskets = Basket::orderBy('id', 'desc')->paginate(10);

		return FruitResponse::makeResponse($baskets);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(StoreBasketRequest $request)
	{

		if (count($items = $request->input('items')))
			if (!FruitValidator::validateBasketWeight($request->input('max'), $items)) {
				return  FruitResponse::makeResponse([], 'Items too heavy.', 400);
			}

		$basket = new Basket($request->only('name', 'max'));
		$basket->save();

		if (count($items)) {
			$basket->addItemsFromRequest($items);
		}
		$basket['items'] = $basket->items()->getEager();
		return FruitResponse::makeResponse($basket, 'Basket created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		$basket = Basket::with('items')->findOrFail($id);
		return FruitResponse::makeResponse($basket);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(UpdateBasketRequest $request, $id)
	{
		$basket = Basket::with('items')->findOrFail($id);

		$basket->update([
			'name' => $request->input("name")
		]);
		return FruitResponse::makeResponse($basket, 'Basket updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$basket = Basket::findOrFail($id);
		$basket->delete();
		return FruitResponse::makeResponse([], 'Basket deleted successfully.');
	}

}
