<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Item;
use App\Basket;

use Illuminate\Http\Request;

use App\FruitValidator;
use App\FruitResponse;

use App\Http\Requests\StoreItemRequest;

class ItemController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @param int $basket_id
	 * @return Response
	 */
	public function store(StoreItemRequest $request, $basket_id)
	{
		$basket = Basket::with('items')->findOrFail($basket_id);
		$items = $request->input('items');

		if (!count($items))
			return FruitResponse::makeResponse([], 'Bad request.', 400);

		if (!FruitValidator::validateBasketWeight($basket->max, array_merge($basket->items()->getEager()->toArray(), $items)))
			return  FruitResponse::makeResponse([], 'Items too heavy.', 400);


		$basket->addItemsFromRequest($items);

		$basket['items'] = $basket->items()->getEager();
		return FruitResponse::makeResponse($basket, 'Items added successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($basket_id, $id)
	{
		$item = Item::findOrFail($id);
		$item->delete();

		return FruitResponse::makeResponse([], 'Item deleted successfully.');
	}

}
