<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreBasketRequest extends StoreItemRequest
{

	protected $rules = [
		'name' => 'required',
		'max' => 'required',
	];

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if (count(\Illuminate\Support\Facades\Input::get('items', [])))
			return array_merge($this->rules, parent::rules());

		return $this->rules;
	}
}
