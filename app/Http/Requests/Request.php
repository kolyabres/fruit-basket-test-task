<?php

namespace App\Http\Requests;

use App\FruitResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;

abstract class Request extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}


	public function response(array $errors)
	{
		return FruitResponse::makeResponse([] ,"Bad request.", 400);
	}
}
