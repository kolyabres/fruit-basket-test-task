<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreItemRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'items'=>'array',
            'items.*.type' => 'required|in:apple,orange,watermelon',
            'items.*.weight' => 'required'
        ];
    }
}
