<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api/v1'], function () {
	Route::resource("baskets", "BasketController", [
		'only' => ['index', 'store', 'show', 'update', 'destroy']
	]);

	Route::resource("baskets.items", "ItemController", [
		'only' => ['store', 'destroy']
	]);
});
Route::get('/', function () {
	return View::make('docs.api.v1.index');
});
