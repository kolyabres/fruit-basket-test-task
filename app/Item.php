<?php

namespace App;



class Item extends Base
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'type', 'weight',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'created_at', 'updated_at','basket_id',
	];

	/**
	 * Get the basket that contains the item
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function basket()
	{
		return $this->belongsTo('App\Basket');
	}
}
