<?php
/**
 * Created by PhpStorm.
 * User: kolyabres
 * Date: 27.05.16
 * Time: 10:36
 */


return [
	'messages' => [
		'store' => 'Item created successfully.',
		'update' => 'Item updated successfully.',
		'destroy' => 'Item deleted successfully.',
	]
];